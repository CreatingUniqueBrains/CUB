# Robotics Workshop

Conducted on 11, 12 and 15 August 2018

## Day 1 

Venue: BOX, UPCYCLE office, Panavila,Trivandrum,Kerala

8:00  - Initial settings and preparing the venue

9:00  - Registrations by participants
* 20 Registrations and 1 spot registration
  
10:00- First session by Vaisakh Anand
 
* Introduction to Robotics
* Microprocessors

11:00-Second session by Venugopal Pai

* Introduction to programming
* Programming languages
* Basic mathematical modelling

12:00- Lunch Break

13:00- Third session by Vaisakh Anand

* Hands on training with Arduino
  
16:00- End of day 1.

## Day 2

Venue: SAHRIDHAYA ,Statue ,Trivandrum, Kerala

10:00 -First session by Akhil S G

* Innovative thinking
* Activity (Group learning about social innovations)
  
* Doccumentation skills 

11:00 -Second session by Vaisakh Anand

* 7 segment LED 
  
12:30 - Lunch break

13:30 - Session by Vaisakh Anand 

* LCD display
* Hands on training
  
16:30 -End of day 2
 
## Day 3

Venue:  BOX, UPCYCLE office, Panavila,Trivandrum,Kerala

10:00 - Day 3 started with some motivational videos

* Change makers
* How  little things can bring huge impacts
  
10:30 : Teams started working on their projects ,components were given as per their request.

13:15 - Lunch break

14:15 - Last hour for the project preparation. 

14:45 - Project presentation by respective teams and (evaluation and suggestions by experts)

16:30 - Presentations come to an end , thoughts and value points by Akhil S G

17:00 - Session by Venugopal Pai
  
* GIT and the revolutions in the programming world

17:45 - Certificates issued to the participants 

18:00 - End of day 3 
